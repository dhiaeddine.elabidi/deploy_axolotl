"""
File configuration.py
Implement logic to deal with app configuration
There is two type of configuration file
-- development.json for development environment
-- production.json for production.json
to set the configuration environment set configuration.json file
"""
import os
import json
import traceback


def configuration(basedir=None):
    """
    Returns a dictionary of the application configuration
    """
    try:
        with open(os.path.join("configuration", "configuration.json")) as f:
            env = json.load(f)
            if env['environment'] == "development":
                with open(os.path.join("configuration", "development.json")) as d:
                    return json.load(d)
            elif env['environment'] == "production":
                with open(os.path.join("configuration", "production.json")) as p:
                    return json.load(p)
            else:
                raise IndexError("Environment value not supported (only production or development are accepted)")
    except FileNotFoundError:
        with open(os.path.join("..", "..", "..", "configuration", "configuration.json")) as f:
            env = json.load(f)
            if env['environment'] == "development":
                with open(os.path.join("..", "..", "..", "configuration", "development.json")) as d:
                    return json.load(d)
            elif env['environment'] == "production":
                with open(os.path.join("..", "..", "..", "configuration", "production.json")) as p:
                    return json.load(p)
            else:
                raise IndexError("Environment value not supported (only production or development are accepted)")
