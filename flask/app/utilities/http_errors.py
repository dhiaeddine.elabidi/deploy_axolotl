"""
File: http_errors.py
Contains dictionary to http status description status
Source: https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
"""


HttpError = {
    401: {
        'code': 401,
        'title': "Unauthorized",
        'description': "The request has not been applied because it lacks valid "
                       "authentication credentials for the target resource."
    },
    403: {
        'code': 403,
        'title': "Forbidden",
        'description': "The request contained valid data and was understood by the server,"
                       " but the server is refusing action. This may be due to the user not"
                       " having the necessary permissions for a resource."
    },
    404: {
        'code': 404,
        'title': "Not Found",
        'description': "The origin server did not find a current representation for "
                       "the target resource or is not "
                       "willing to disclose that one exists."
    },
    405: {
        'code': 405,
        'title': "Method Not Allowed",
        'description': "The method received in the request-line is known by the origin"
                       " server but not supported by the target resource."
    },
    422: {
        'code': 422,
        'title': "Unprocessable Entity",
        'description': "The request was well-formed but was unable to be followed due to semantic errors."
    },
    428: {
        'code': 428,
        'title': "Precondition Required",
        'description': "The origin server requires the request to be conditional. "
                       "Intended to prevent the 'lost update' problem, where a client GETs a resource's state,"
                       " modifies it, and PUTs it back to the server, when meanwhile a third party has modified"
                       " the state on the server, leading to a conflict."
    },
    500: {
        'code': 500,
        'title': "Internal Server Error",
        'description': "The server encountered an unexpected condition that prevented "
                       "it from fulfilling the request."
    },
    501: {
        'code': 501,
        'title': "Not Implemented",
        'description': "The server does not support the functionality required to fulfill the request."
    }
}
