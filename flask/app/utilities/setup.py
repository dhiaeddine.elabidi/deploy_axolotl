"""
File: setup.py
Contains application specific identification
"""
from datetime import datetime

setup = {
    'application': {
        'name': "Axolotl",
        'acronym': "xltl",
        'description': "Yootel intelligent customers manager",
        'version': "0.0.0",
        'release_date': "Mars 2020",
        'copyright': "Copyright Ⓒ 2019-" + str(datetime.now().year) + ". All rights are reserved.",
        'authors': [
            {
                'name': "Mehdi Ben Hamida",
                'organism': "Yootel",
                'tel': "+336 05 95 74 61",
                'email': "mbenhamida@yooth-it.com",
            },
        ]
    },
    'company': {
        'name': "YOOTEL",
        'address': "55, Rue faubourg montmartre",
        'zip': "75009",
        'city': "Paris",
        'email': "contact@yootel.io",
        'tel': "+330 00 00 00 00",
        'website': "https://yootel.io"
    },
    'support': {
            'mail': "info@yootel.io",
            'tel_1': "01 85 88 55 55",
            'tel_2': "",
            'address': "55, Rue Du Faubourg Montmartre 75009 PARIS"
    }
}
