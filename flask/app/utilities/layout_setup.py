"""
File: layout_setup.py
Setup and variable for the layout page
"""
from app.utilities.setup import setup
from app.utilities.choices import supported_languages
from app.utilities.translate import translate

layout = {
    'title': setup['application']['name'],
    'version': setup['application']['version'],
    'name': setup['application']['name'],
    'copyright': setup['application']['copyright'],
    'company_name': setup['company']['name'],
    'company_address': setup['company']['address'],
    'company_city': setup['company']['city'],
    'company_zip': setup['company']['zip'],
    'company_website': setup['company']['website'],
    'company_email': setup['company']['email'],
    'company_phone': setup['company']['tel'],
    'langs': supported_languages(),
    'translate': translate
}
