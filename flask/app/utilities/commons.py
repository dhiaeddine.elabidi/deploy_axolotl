"""
File: commons.py
Contains application specific identification
"""
from datetime import datetime
import hashlib
import random
import string
import uuid

# constants declaration


# common functions declaration
def get_unique_id():
    """
    Generate a random identifier with uuid 4 form
    :return: string of uuid 4
    """
    return str(uuid.uuid4())


def hashing(st):
    """
    sha 224 hashing function
    :param st: input string (to hash)
    :return: the hashed string
    """
    return str(hashlib.sha224(st.encode('utf-8')).hexdigest())


def generate_password(length=15):
    """
    Generate a random string (letters and digits) to be user as a random password
    :param length: length of the random string, default 35
    :return: random string
    """
    p = string.ascii_letters + string.digits
    return ''.join(random.choice(p) for _ in range(length))


def verify_fields(*argv):
    for field in argv:
        if len(field) < 8 or len(field) > 64:
            return False
    return True


def generate_invoice_ref(service_code="xxxx"):
    if len(service_code) != 4:
        raise ValueError("service_code string length is different from 4")
    today = datetime.utcnow()
    day = str(today.day)
    month = str(today.month)
    year = str(today.year)[2:]
    random_str = ''.join(random.choice(string.digits) for _ in range(6))
    ref = "IN_"+service_code+"_"+year+month+day+"_"+random_str
    return ref


def db_format_date(month, year):
    if month == '2':
        if int(year) % 4 == 0:
            return {
                'start_date': year+"-02-01",
                'end_date': year+"-02-29"
            }
        else:
            return {
                'start_date': year+"-02-01",
                'end_date': year+"-02-28"
            }
    elif month in ['1', '3', '5', '7', '8', '10', '12']:
        return {
            'start_date': year + "-0"+month+"-01",
            'end_date': year + "-0"+month+"-31"
        }
    elif month in ['4', '6', '9', '11']:
        return {
            'start_date': year + "-0"+month+"-01",
            'end_date': year + "-0"+month+"-30"
        }
    else:
        raise IndexError(month+"out of range [1..12]")


def format_time(seconds):
    def leading_zero(i):
        if isinstance(i, str):
            return i
        if i < 10:
            return "0"+str(i)
        else:
            return str(i)
    hours = 0
    minutes = 0
    if seconds >= 60:
        minutes = seconds//60
        seconds = seconds%60
        if minutes >= 60:
            hours = minutes//60
            minutes = minutes%60
    return leading_zero(hours)+":"+leading_zero(minutes)+":"+leading_zero(seconds)
