"""
File: choices.py
Get choices to fill select choices in forms
"""
from phonenumbers import COUNTRY_CODE_TO_REGION_CODE
import phonenumbers


def supported_languages():
    return [
        ('gb', "English (UK)"),
        ('us', "English (US)"),
        ('fr', "Français (FR)"),
        ('de', "Deutsche (DE)"),
    ]


def counties_code(default_phone=None):
    result = []
    for code, countries in COUNTRY_CODE_TO_REGION_CODE.items():
        for country in countries:
            if code not in [800, 808, 833, 844, 855, 866, 870, 877, 878, 881, 882, 883, 979, 888]:
                result.append((str(code), country + " (+" + str(code) + ")"))
    if default_phone is None:
        return sorted(result, key=lambda x: x[1])
    else:
        code = phonenumbers.parse(default_phone, None).__dict__['country_code']
        return [(str(code), COUNTRY_CODE_TO_REGION_CODE[code][0] +
                 " (+" + str(code) + ")")] + sorted(result, key=lambda x: x[1])


def topology_code():
    rates = {
        "G000": {
            "rate_call": 0.0,
            "rate_minute": 0.0
        },
        "B000": {
            "rate_call": 0.0,
            "rate_minute": 0.0
        },
        "A005": {
            "rate_call": 0.05,
            "rate_minute": 0.0
        },
        "A008": {
            "rate_call": 0.08,
            "rate_minute": 0.0
        },
        "A009": {
            "rate_call": 0.09,
            "rate_minute": 0.0
        },
        "A010": {
            "rate_call": 0.1,
            "rate_minute": 0.0
        },
        "A012": {
            "rate_call": 0.12,
            "rate_minute": 0.0
        },
        "A015": {
            "rate_call": 0.15,
            "rate_minute": 0.0
        },
        "A020": {
            "rate_call": 0.2,
            "rate_minute": 0.0
        },
        "A030": {
            "rate_call": 0.3,
            "rate_minute": 0.0
        },
        "A040": {
            "rate_call": 0.4,
            "rate_minute": 0.0
        },
        "A050": {
            "rate_call": 0.5,
            "rate_minute": 0.0
        },
        "A060": {
            "rate_call": 0.6,
            "rate_minute": 0.0
        },
        "A065": {
            "rate_call": 0.65,
            "rate_minute": 0.0
        },
        "A079": {
            "rate_call": 0.79,
            "rate_minute": 0.0
        },
        "A080": {
            "rate_call": 0.8,
            "rate_minute": 0.0
        },
        "A090": {
            "rate_call": 0.9,
            "rate_minute": 0.0
        },
        "A099": {
            "rate_call": 0.99,
            "rate_minute": 0.0
        },
        "A100": {
            "rate_call": 1.0,
            "rate_minute": 0.0
        },
        "A150": {
            "rate_call": 1.5,
            "rate_minute": 0.0
        },
        "A199": {
            "rate_call": 1.99,
            "rate_minute": 0.0
        },
        "A200": {
            "rate_call": 2.0,
            "rate_minute": 0.0
        },
        "A250": {
            "rate_call": 2.5,
            "rate_minute": 0.0
        },
        "A299": {
            "rate_call": 2.99,
            "rate_minute": 0.0
        },
        "A300": {
            "rate_call": 3.0,
            "rate_minute": 0.0
        },
        "D005": {
            "rate_call": 0.0,
            "rate_minute": 0.05
        },
        "D006": {
            "rate_call": 0.0,
            "rate_minute": 0.06
        },
        "D009": {
            "rate_call": 0.0,
            "rate_minute": 0.09
        },
        "D010": {
            "rate_call": 0.0,
            "rate_minute": 0.1
        },
        "D012": {
            "rate_call": 0.0,
            "rate_minute": 0.12
        },
        "D015": {
            "rate_call": 0.0,
            "rate_minute": 0.15
        },
        "D018": {
            "rate_call": 0.0,
            "rate_minute": 0.18
        },
        "D020": {
            "rate_call": 0.0,
            "rate_minute": 0.2
        },
        "D025": {
            "rate_call": 0.0,
            "rate_minute": 0.25
        },
        "D030": {
            "rate_call": 0.0,
            "rate_minute": 0.3
        },
        "D035": {
            "rate_call": 0.0,
            "rate_minute": 0.35
        },
        "D040": {
            "rate_call": 0.0,
            "rate_minute": 0.4
        },
        "D045": {
            "rate_call": 0.0,
            "rate_minute": 0.45
        },
        "D050": {
            "rate_call": 0.0,
            "rate_minute": 0.5
        },
        "D060": {
            "rate_call": 0.0,
            "rate_minute": 0.6
        },
        "D070": {
            "rate_call": 0.0,
            "rate_minute": 0.7
        },
        "D080": {
            "rate_call": 0.0,
            "rate_minute": 0.8
        },
        "M001": {
            "rate_call": 0.08,
            "rate_minute": 0.02
        },
        "M002": {
            "rate_call": 0.12,
            "rate_minute": 0.04
        },
        "M003": {
            "rate_call": 1.12,
            "rate_minute": 0.11
        },
        "M004": {
            "rate_call": 1.35,
            "rate_minute": 0.34
        },
        "M005": {
            "rate_call": 1.49,
            "rate_minute": 1.49
        },
        "M006": {
            "rate_call": 1.57,
            "rate_minute": 0.45
        },
        "M007": {
            "rate_call": 1.89,
            "rate_minute": 0.5
        },
        "M008": {
            "rate_call": 1.99,
            "rate_minute": 0.5
        },
        "M009": {
            "rate_call": 1.99,
            "rate_minute": 0.99
        },
        "M010": {
            "rate_call": 1.99,
            "rate_minute": 1.99
        },
        "M011": {
            "rate_call": 2.19,
            "rate_minute": 0.5
        },
        "M012": {
            "rate_call": 2.19,
            "rate_minute": 2.19
        },
        "M013": {
            "rate_call": 2.5,
            "rate_minute": 0.5
        },
        "M014": {
            "rate_call": 2.5,
            "rate_minute": 0.99
        },
        "M015": {
            "rate_call": 2.5,
            "rate_minute": 2.5
        },
        "M016": {
            "rate_call": 2.75,
            "rate_minute": 2.75
        },
        "M017": {
            "rate_call": 2.99,
            "rate_minute": 0.99
        },
        "M018": {
            "rate_call": 2.99,
            "rate_minute": 1.99
        },
        "M019": {
            "rate_call": 2.99,
            "rate_minute": 2.99
        },
        "M020": {
            "rate_call": 3.25,
            "rate_minute": 3.25
        },
        "M021": {
            "rate_call": 3.5,
            "rate_minute": 3.5
        },
        "M022": {
            "rate_call": 2.9,
            "rate_minute": 0.5
        }
    }
    return {
        'rates': rates,
        'choices': [(code, code) for code in rates]
    }