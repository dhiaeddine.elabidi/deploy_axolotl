"""
File: invoice.py
Invoice generated details
"""
from app.models.database.tables.rsva_invoice import RsvaInvoice
from app import db
from datetime import datetime


class RsvaNumberInvoice(db.Model):
    """
    Class Invoice: Object for database mapping
    sql_alchemy model
    """
    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    num_calls = db.Column(db.Integer, unique=False, nullable=False, default=0)
    duration_calls = db.Column(db.Integer, unique=False, nullable=False, default=0)
    value = db.Column(db.Float, nullable=False, unique=False, default=0.0)
    period_month = db.Column(db.Integer, unique=False, nullable=False)
    period_year = db.Column(db.Integer, unique=False, nullable=False)
    last_modified = db.Column(db.DateTime, default=datetime.utcnow)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # foreign keys
    number_id = db.Column(db.String(36), db.ForeignKey('number.id'), nullable=False, unique=False)
    # relationship
    rsva_number_invoice_rsva_invoice = db.relationship('RsvaInvoice', backref='rsva_number_invoice_rsva_invoice')
