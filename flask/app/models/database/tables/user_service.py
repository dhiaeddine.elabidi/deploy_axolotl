"""
File: user_service.py
UserService database model
"""
from app import db
from datetime import datetime


class UserService(db.Model):
    """
    UserService class: Object for database
    sql_alchemy model
    """
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String(36), db.ForeignKey('user.id'), nullable=False, unique=False)
    service_id = db.Column(db.Integer, db.ForeignKey('service.id'), nullable=False, unique=False)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
