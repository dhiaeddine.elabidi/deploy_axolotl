"""
File: tariff_enduser.py
Final end user tariff
"""
from app import db
from datetime import datetime
from app.models.database.tables.rsva_tariff import RsvaTariff


class RsvaTariffEndUser(db.Model):
    """
    Class TariffEndUser: Object for database mapping
    sql_alchemy model
    """
    code = db.Column(db.String(6), primary_key=True, unique=True, nullable=False)
    call_rate = db.Column(db.Float, nullable=False, unique=False, default=0.0)
    minute_rate = db.Column(db.Float, nullable=False, unique=False, default=0.0)
    last_modified = db.Column(db.DateTime, default=None)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # relationship
    rsva_tariff_end_user_tariff = db.relationship('RsvaTariff', backref='rsva_tariff_end_user_tariff')


