"""
File: ticket.py
User database model
"""
from app import db
from datetime import datetime


class Ticket(db.Model):
    """
    Class Ticket: Object for database mapping
    sql_alchemy model
    """
    id = db.Column(db.Integer, primary_key=True)
    subject = db.Column(db.String(64))
    message = db.Column(db.String(750))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # foreign keys
    user_id = db.Column(db.String(36), db.ForeignKey('user.id'), nullable=False, unique=False)
