"""
File: currency.py
Currency database model
"""
from app import db
from datetime import datetime


class Currency(db.Model):
    """
    Currency class: Object for database mapping
    sql_alchemy model
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    iso_code = db.Column(db.String(5), nullable=False)
    icon = db.Column(db.String(20), nullable=True, default=None)
    exchange_rate = db.Column(db.Float, nullable=False)
    last_modified = db.Column(db.DateTime, default=datetime.utcnow)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # relationship
    currency_user = db.relationship('User', backref='currency_user')


