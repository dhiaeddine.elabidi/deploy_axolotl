"""
File: client_invoice.py
ClientInvoice mapping model
"""
from app.models.database.tables.rsva_invoice import RsvaInvoice
from app import db
from datetime import datetime


class RsvaUserInvoice(db.Model):
    """
    Class ClientInvoice
    """
    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    num_calls = db.Column(db.Integer, unique=False, nullable=False, default=0)
    duration_calls = db.Column(db.Integer, unique=False, nullable=False, default=0)
    pre_value = db.Column(db.Float, nullable=False, unique=False, default=0.0)
    discount = db.Column(db.Float, nullable=True, default=None)
    vat = db.Column(db.Float, nullable=True, default=None)
    value = db.Column(db.Float, nullable=True, default=None)
    period_month = db.Column(db.Integer, unique=False, nullable=False)
    period_year = db.Column(db.Integer, unique=False, nullable=False)
    paid = db.Column(db.Boolean, nullable=False, default=False)
    user_id = db.Column(db.String(36), db.ForeignKey('user.id'), nullable=False, unique=False)
    last_modified = db.Column(db.DateTime, default=datetime.utcnow)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # relationship
    rsva_user_invoice_rsva_invoice = db.relationship('RsvaInvoice', backref='rsva_user_invoice_rsva_invoice')
    __tables_args__ = (
        db.CheckConstraint('discount<1'),
        db.CheckConstraint('discount>0'),
        db.CheckConstraint('vat<1'),
        db.CheckConstraint('vat>0'),
    )
