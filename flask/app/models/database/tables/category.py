"""
File: category.py
Category database model
"""
from app import db
from datetime import datetime
from app.models.database.tables.user import User


class Category(db.Model):
    """
    Class category: Object for database mapping
    sql_alchemy_model
    """
    id = db.Column(db.Integer, primary_key=True, unique=True)
    code = db.Column(db.String(5), unique=True, nullable=False)
    name = db.Column(db.String(50), unique=True, nullable=False)
    description = db.Column(db.String(100), unique=False, nullable=True, default=None)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # relationships
    category_user = db.relationship('User', backref='category_user')

