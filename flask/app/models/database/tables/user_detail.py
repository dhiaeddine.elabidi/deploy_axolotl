"""
File: user_detail.py
User detail database model
"""

from app import db
from datetime import datetime


class UserDetail(db.Model):
    """
    Class UserDetail: object for database mapping
    sql_alchemy model
    """
    id = db.Column(db.Integer, primary_key=True, unique=True)
    first_name = db.Column(db.String(64), nullable=False, unique=False)
    last_name = db.Column(db.String(64), nullable=False, unique=False)
    organism = db.Column(db.String(35), nullable=True, unique=False)
    email = db.Column(db.String(64), nullable=False, unique=False)
    country_code = db.Column(db.String(10), nullable=True, unique=False)
    number = db.Column(db.String(64), nullable=True, unique=False)
    address_1 = db.Column(db.String(64), nullable=True, unique=False)
    address_2 = db.Column(db.String(64), nullable=True, unique=False)
    city = db.Column(db.String(20), nullable=True, unique=False)
    country = db.Column(db.String(50), nullable=True)
    zip = db.Column(db.String(16), nullable=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # foreign keys
    user_id = db.Column(db.String(36), db.ForeignKey('user.id'), nullable=False, unique=True)
