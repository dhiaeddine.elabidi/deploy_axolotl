"""
File: tariff_client.py
Client oriented tariff model
"""
from app.models.database.tables.rsva_tariff import RsvaTariff
from app import db
from datetime import datetime


class RsvaTariffClient(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    call_rate = db.Column(db.Float, nullable=False, unique=False, default=0.0)
    minute_rate = db.Column(db.Float, nullable=False, unique=False, default=0.0)
    interval_1 = db.Column(db.Integer, nullable=False, unique=False, default=1)
    interval_n = db.Column(db.Integer, nullable=False, unique=False, default=1)
    price_interval_1 = db.Column(db.Float, nullable=False, unique=False, default=0.0)
    price_interval_n = db.Column(db.Float, nullable=False, unique=False, default=0.0)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    # relationship
    rsva_tariff_client_tariff = db.relationship('RsvaTariff', backref='rsva_tariff_client_tariff')
