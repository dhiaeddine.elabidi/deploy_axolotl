"""
File: user_model.py
UserModel model
"""
from app.models.database.tables.user import User
from app.models.database.tables.user_detail import UserDetail
from app.models.database.tables.category import Category
from app.models.database.tables.currency import Currency
from app.utilities.choices import supported_languages
from app.utilities.commons import get_unique_id, generate_password
from app import db
from datetime import datetime


class UserModel:
    """
    The model of user
    """

    @staticmethod
    def add_user(username, first_name, last_name, email, country_code, number, organism):
        ref = get_unique_id()
        password = generate_password()
        user = User(
            id=ref,
            username=username,
            password=password,
            valid=False,
            first_login=True,
            category_id=2,
            currency_id=1
        )
        details = UserDetail(
            first_name=first_name,
            last_name=last_name,
            organism=organism,
            email=email,
            country_code=country_code,
            number=number,
            user_id=ref
        )
        db.session.add(user)
        db.session.add(details)
        db.session.commit()
        return ref, password

    @staticmethod
    def get_user(user_id=None):
        if user_id is None:
            raise TypeError("user_id cannot be None.")
        else:
            user = db.session.query(User, Category, Currency, UserDetail)\
                .join(UserDetail)\
                .join(Category)\
                .join(Currency)\
                .filter(User.id == user_id)\
                .first()
            if user:
                return {
                    'id': user[0].id,
                    'username': user[0].username,
                    'lang': user[0].lang,
                    'valid': user[0].valid,
                    'last_login': user[0].last_login,
                    'first_name': user[3].first_name,
                    'last_name': user[3].last_name,
                    'organism': user[3].organism,
                    'created_at': user[3].created_at,
                    'contact': {
                        'email': user[3].email,
                        'phone': {
                            'country_code': user[3].country_code,
                            'number': user[3].number
                        },
                        'address_1': user[3].address_1,
                        'address_2': user[3].address_2,
                        'city': user[3].city,
                        'country': user[3].country,
                        'zip': user[3].zip
                    },
                    'currency': {
                        'name': user[2].name,
                        'iso_code': user[2].iso_code,
                        'icon': user[2].icon,
                        'exchange_rate': user[2].exchange_rate,
                    },
                    'category': {
                        'code': user[1].code,
                        'name': user[1].name,
                        'description': user[1].description
                    }
                }
            else:
                raise IndexError("user_id does not exist.")

    @staticmethod
    def get_users():
        users = db.session.query(User, Category, Currency, UserDetail) \
            .join(UserDetail) \
            .join(Category) \
            .join(Currency) \
            .all()
        results = list()
        for user in users:
            results.append(
                {
                    'username': user[0].username,
                    'reference': user[0].id,
                    'lang': user[0].lang,
                    'valid': user[0].valid,
                    'last_login': user[0].last_login,
                    'first_name': user[3].first_name,
                    'last_name': user[3].last_name,
                    'organism': user[3].organism,
                    'created_at': user[3].created_at,
                    'contact': {
                        'email': user[3].email,
                        'phone': {
                            'country_code': user[3].country_code,
                            'number': user[3].number
                        },
                        'address_1': user[3].address_1,
                        'address_2': user[3].address_2,
                        'city': user[3].city,
                        'country': user[3].country,
                        'zip': user[3].zip
                    },
                    'currency': {
                        'name': user[2].name,
                        'iso_code': user[2].iso_code,
                        'icon': user[2].icon,
                        'exchange_rate': user[2].exchange_rate,
                    },
                    'category': {
                        'code': user[1].code,
                        'name': user[1].name,
                        'description': user[1].description
                    }
                }
            )
        return results

    @staticmethod
    def get_membership_requests():
        users = db.session.query(User, Category, Currency, UserDetail) \
            .join(UserDetail) \
            .join(Category) \
            .join(Currency) \
            .filter(User.valid == False) \
            .filter(User.first_login == True) \
            .all()
        results = list()
        for user in users:
            results.append(
                {
                    'reference': user[0].id,
                    'username': user[0].username,
                    'password': user[0].password,
                    'lang': user[0].lang,
                    'valid': user[0].valid,
                    'last_login': user[0].last_login,
                    'first_name': user[3].first_name,
                    'last_name': user[3].last_name,
                    'organism': user[3].organism,
                    'created_at': user[3].created_at,
                    'contact': {
                        'email': user[3].email,
                        'phone': {
                            'country_code': user[3].country_code,
                            'number': user[3].number
                        },
                        'address_1': user[3].address_1,
                        'address_2': user[3].address_2,
                        'city': user[3].city,
                        'country': user[3].country,
                        'zip': user[3].zip
                    },
                    'currency': {
                        'name': user[2].name,
                        'iso_code': user[2].iso_code,
                        'icon': user[2].icon,
                        'exchange_rate': user[2].exchange_rate,
                    },
                    'category': {
                        'code': user[1].code,
                        'name': user[1].name,
                        'description': user[1].description
                    }
                }
            )
        return results

    @staticmethod
    def delete_user(user_id):
        User.query.filter_by(id=user_id).delete()
        db.session.commit()

    @staticmethod
    def accept_user(user_id):
        User.query.filter_by(id=user_id).update({'valid': True})
        db.session.commit()

    @staticmethod
    def set_language(user_id, lang):
        n = len([item for item in supported_languages() if lang in item])
        if n == 1:
            user = User.query.filter_by(id=user_id).first()
            user.lang = lang
            user.last_modified = datetime.utcnow()
            db.session.commit()
        else:
            raise IndexError("Language is not supported.")

    @staticmethod
    def set_currency(user_id, currency_id):
        # check if currency exist
        currency = Currency.query.filter_by(id=currency_id).first()
        if currency:
            user = User.query.filter_by(id=user_id).first()
            user.currency_id = currency_id
            user.last_modified = datetime.utcnow()
            db.session.commit()
        else:
            raise IndexError("currency_id does not exist.")
