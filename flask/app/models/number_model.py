"""
File: number_model.py
NumberModel model
"""
from app.models.database.tables.number import Number
from app.models.database.tables.user import User
from app.models.database.tables.rsva_tariff_end_user import RsvaTariffEndUser
from app.models.database.tables.rsva_tariff_client import RsvaTariffClient
from app.models.database.tables.rsva_tariff import RsvaTariff
from app.models.database.tables.rsva_number_invoice import RsvaNumberInvoice
from app.utilities.commons import get_unique_id
from app import db
from phone_iso3166.country import *
from datetime import datetime
import phonenumbers
import pycountry


class NumberModel:
    """
    The model of number
    """

    @staticmethod
    def add(number):
        country_code = number['country_code']
        phone_number = number['phone_code']
        c = pycountry.countries.get(alpha_2=phone_country(country_code))
        x = phonenumbers.parse(country_code + phone_number, c.alpha_2)
        client_tariff = RsvaTariffClient(
            call_rate=number['client_tariff']['call_rate'],
            minute_rate=number['client_tariff']['price_interval_n'],
            interval_1=number['client_tariff']['interval_1'],
            interval_n=number['client_tariff']['interval_n'],
            price_interval_1=number['client_tariff']['price_interval_1'],
            price_interval_n=number['client_tariff']['price_interval_n']
        )
        db.session.add(client_tariff)
        db.session.flush()
        tariff_client_id = client_tariff.id
        num_ref = get_unique_id()
        tariff_ref = get_unique_id()
        tariff = RsvaTariff(
            id=tariff_ref,
            rsva_tariff_end_user_code=number['end_user_tariff'],
            rsva_tariff_client_id=tariff_client_id
        )
        db.session.add(tariff)
        new_number = Number(
            id=num_ref,
            country=c.alpha_2,
            country_fullname=c.name,
            country_code=x.country_code,
            number=x.national_number,
            rsva_tariff_id=tariff_ref
        )
        db.session.add(new_number)
        db.session.commit()
        return num_ref

    @staticmethod
    def get_number(number_id):
        number = db.session.query(RsvaTariff, Number, RsvaTariffClient, RsvaTariffEndUser)\
            .join(Number)\
            .join(RsvaTariffClient)\
            .join(RsvaTariffEndUser)\
            .filter(Number.id == number_id)\
            .first()
        return {
            'country_code': number[1].country_code,
            'number': number[1].number,
            'country': number[1].country,
            'country_fullname': number[1].country_fullname,
            'tariff_end_user': {
                'code': number[3].code,
                'call_rate': number[3].call_rate,
                'minute_rate': number[3].minute_rate
            },
            'tariff_client': {
                'call_rate': number[2].call_rate,
                'minute_rate': number[2].minute_rate,
                'interval_1': number[2].interval_1,
                'interval_n': number[2].interval_n,
                'price_interval_1': number[2].price_interval_1,
                'price_interval_n': number[2].price_interval_n
            }
        }

    @staticmethod
    def get_numbers(user_id):
        numbers = Number.query.filter_by(user_id=user_id).all()
        results = list()
        for number in numbers:
            results.append({
                'id': number.id,
                'country': number.country,
                'country_fullname': number.country_fullname,
                'country_code': number.country_code,
                'number': number.number
            })
        return results

    @staticmethod
    def attach_user(number_id, user_id):
        user = User.query.filter_by(id=user_id)
        if user:
            number = Number.query.filter_by(id=number_id).first()
            if number:
                number.user_id = user_id
                number.last_modified = datetime.utcnow()
                db.session.commit()
            else:
                raise IndexError("number_id does not exist")
        else:
            raise IndexError("user_id does not exist")

    @staticmethod
    def detach_user(number_id):
        number = Number.query.filter_by(id=number_id).first()
        if number:
            number.user_id = None
            number.last_modified = datetime.utcnow()
            db.session.commit()
        else:
            raise IndexError("number_id does not exist.")


