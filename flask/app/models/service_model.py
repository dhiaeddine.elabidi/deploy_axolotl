"""
File: service_model.py
ServiceModel model
"""
from app.models.database.tables.user import User
from app.models.database.tables.user_service import UserService
from app.models.database.tables.service import Service
from app import db
from datetime import datetime


class ServiceModel:
    """
    The model of service
    """

    @staticmethod
    def add_service(code, name, level, icon, description):
        # verify code does not exist already
        exist_service = Service.query.filter_by(service_code=code).first()
        if exist_service:
            raise IndexError("service_code already exist.")
        else:
            service = Service(
                service_code=code,
                name=name,
                level=level,
                icon=icon,
                description=description
            )
            db.session.add(service)
            db.session.commit()

    @staticmethod
    def attach_user_service(user_id, service_code):
        # verify user_id exist
        user = User.query.filter_by(id=user_id).first()
        if user:
            # verify service_code exist
            service = Service.query.filter_by(service_code=service_code).first()
            if service:
                user_service = UserService(user_id=user_id, service_id=service.id)
                db.session.add(user_service)
                db.session.commit()
            else:
                raise IndexError("service_code does not exist.")
        else:
            raise IndexError("user_id does not exist.")

    @staticmethod
    def detach(user_id, service_code):
        # verify user and service exist
        user = User.query.filter_by(id=user_id).first()
        service = Service.query.filter_by(service_code=service_code).first()
        if user and service:
            UserService.query.filter_by(user_id=user_id, service_id=service.id).delete()
            db.session.commit()
        else:
            raise IndexError("Cannot detach service.")

    @staticmethod
    def get_user_services(user_id=None):
        if user_id is None:
            raise TypeError("user_id cannot be None.")
        else:
            user_services = db.session.query(UserService, User, Service)\
                .join(User)\
                .join(Service)\
                .filter(User.id == user_id)\
                .all()
            services = list()
            for service in user_services:
                services.append({
                    'code': service[2].service_code,
                    'name': service[2].name,
                    'level': service[2].level,
                    'icon': service[2].icon,
                    'link': "/service/set/"+service[2].service_code,
                    'description': service[2].description
                })
            return services
