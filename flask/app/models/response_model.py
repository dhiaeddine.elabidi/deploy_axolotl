"""
File: response_model.py
Schema to access response schema
"""
from app import ma


class SuccessResponseSchema(ma.Schema):
    """
    The main index schema in the application
    Inherits from the ma.Schema
    """
    class Meta:
        fields = ('status', 'code', 'data')


class ErrorResponseSchema(ma.Schema):
    """
    The Error Response Message schema
    Inherits from ma.Schema
    """
    class Meta:
        fields = ('status', 'code', 'data')


# instantiation the schema
success_response = SuccessResponseSchema()
error_response = ErrorResponseSchema()
