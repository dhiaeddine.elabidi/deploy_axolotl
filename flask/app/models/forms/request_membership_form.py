"""
File: request_membership_form.py
Request membership form for the application.
"""
from app.utilities.choices import counties_code
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, BooleanField, validators


class RequestMembershipForm(FlaskForm):
    """
    Request membership form model
    """
    username = StringField('Username', validators=[validators.Length(min=4, max=64), validators.input_required()])
    first_name = StringField('First name', validators=[validators.Length(min=4, max=64),
                                                       validators.input_required()])
    last_name = StringField('Last name', validators=[validators.Length(min=4, max=64),
                                                     validators.input_required()])
    country_code = SelectField('Country code', choices=counties_code())
    number = StringField('Number', validators=[validators.Length(min=4, max=64), validators.input_required()])
    email = StringField('Email', validators=[validators.Length(min=4, max=64),
                                             validators.input_required()])
    organism = StringField('Organism', validators=[validators.Length(min=4, max=35),
                                                   validators.input_required()])
    agreement = BooleanField('Agreement')

