"""
File: login_form.py
Login form for the application
"""
from flask_wtf import FlaskForm
from wtforms import BooleanField, StringField, PasswordField, validators


class LoginForm(FlaskForm):
    """
    Login form model
    """
    username = StringField('Username', validators=[validators.Length(min=4, max=64)])
    password = PasswordField('Password', validators=[validators.Length(min=8, max=80)])
    remember_me = BooleanField('Remember me')
