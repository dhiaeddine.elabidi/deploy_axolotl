"""
define the main application  setup
"""
from app.utilities.configuration import configuration
from flask import Flask
from flask_bootstrap import Bootstrap
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    from flask_marshmallow import Marshmallow
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import os

# create the flask app here
app = Flask(__name__)

# add bootstrap initialization
Bootstrap(app)

# make the base directory for the application
basedir = os.path.abspath(os.path.dirname(__file__))
# get the application configuration
config = configuration()

app.template_folder = os.path.join("views", "pages")
app.static_folder = os.path.join("views", "static")

# app config
app.config['SQLALCHEMY_DATABASE_URI'] = config['database']['connection_string']
# silence the deprecation warning
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'you-will-never-guess_axolotl'

# initialize marshmallow
ma = Marshmallow(app)

# initialize sql_alchemy
db = SQLAlchemy(app)

# default CORS policy
cors = CORS(app, resources={r"/*": {"origins": "*"}})

# initialize login manager with the flask application
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = '/login'

# import the blueprint

# api
from app.controllers.api.index import api_index
from app.controllers.api.billing.rsva import api_billing_rsva
# web
from app.controllers.web.index import web_index
from app.controllers.web.example import web_example
from app.controllers.web.authentication import web_authentication
from app.controllers.web.language import web_language
from app.controllers.web.profile import web_profile
from app.controllers.web.dashboard import web_dashboard
from app.controllers.web.extra import web_extra
from app.controllers.web.service import web_service
from app.controllers.web.invoices import web_invoices
from app.controllers.web.http_errors import web_http_error
from app.controllers.web.admin.dashboard import web_admin_dashboard
from app.controllers.web.admin.users import web_admin_users

# register the blueprints

# api blueprints
if config['application']['api']:
    app.register_blueprint(api_index, url_prefix='/api')
    app.register_blueprint(api_billing_rsva, url_prefix='/api/billing/rsva')

# web blueprints
if config['application']['web']:
    app.register_blueprint(web_index)
    app.register_blueprint(web_example, url_prefix='/example')
    app.register_blueprint(web_authentication)
    app.register_blueprint(web_language, url_prefix='/language')
    app.register_blueprint(web_profile, url_prefix='/profile')
    app.register_blueprint(web_dashboard, url_prefix='/dashboard')
    app.register_blueprint(web_service, url_prefix='/service')
    app.register_blueprint(web_invoices, url_prefix='/invoices')
    app.register_blueprint(web_extra)
    app.register_blueprint(web_http_error, url_prefix='/error')
    app.register_blueprint(web_admin_dashboard, url_prefix='/admin/dashboard')
    app.register_blueprint(web_admin_users, url_prefix='/admin/users')
