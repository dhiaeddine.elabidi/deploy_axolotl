"""
File: service.py
Service web application route definition
"""
from app.models.user_model import UserModel
from app.utilities.logger import *
from flask import Blueprint, render_template, request, redirect, session
from flask_login import login_required, current_user
import traceback


# create a new blueprint for the controller
web_service = Blueprint('web_service', __name__)


# define the routes
@web_service.route('/set/<string:service>', methods=['GET'])
@login_required
def set_service(service):
    """
    Set service route
    :param service:
    :return:
    """
    try:
        user = UserModel.get_user(current_user.id)
        if user['category']['code'] == 'user':
            session['service'] = service
            return redirect('/dashboard/'+service)
        else:
            return redirect('/error/403')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')
