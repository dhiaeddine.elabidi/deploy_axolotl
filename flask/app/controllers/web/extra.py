"""
File: extra.py
Extra feature pages web application route definition
"""
from app.utilities.layout_setup import layout
from app.utilities.menu import get_menu
from app.utilities.logger import *
from app.models.user_model import UserModel
from app.models.service_model import ServiceModel
from flask import Blueprint, render_template, request, redirect, session
from flask_login import login_required, current_user
import traceback


# create a new blueprint for the controller
web_extra = Blueprint('web_extra', __name__)


# define the route
@web_extra.route('/settings', methods=['GET'])
@login_required
def settings_page():
    """
    Settings web application route call
    :return: Settings web page
    """
    try:
        page = {
            'layout': layout,
            'user': UserModel.get_user(current_user.id),
            'title': "Settings",
            'menu': get_menu(session['service']),
            'services': ServiceModel.get_user_services(current_user.id)
        }
        return render_template('/basics/settings.html',
                               page=page)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_extra.route('/help', methods=['GET'])
@login_required
def help_page():
    """
    Help web application route call
    :return: Help web page
    """
    try:
        page = {
            'layout': layout,
            'user': UserModel.get_user(current_user.id),
            'title': "Help",
            'menu': get_menu(session['service']),
            'services': ServiceModel.get_user_services(current_user.id)
        }
        return render_template('/basics/help.html',
                               page=page)
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')


@web_extra.route('/terms', methods=['GET'])
def terms_page():
    try:
        return render_template('/extra/terms.html')
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return redirect('/error/500')
