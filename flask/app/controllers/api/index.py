"""
File: index.py
Default api routes definition
"""
from app.utilities.setup import setup
from app.utilities.logger import *
from app.models.response_model import *
from flask import Blueprint, request
import traceback


# create a new blueprint for the controller
api_index = Blueprint('api_index', __name__)


# define the routes
@api_index.route('/', methods=['GET'])
@api_index.route('/index', methods=['GET'])
def index():
    """
    Basic api route call
    Return api information
    """
    try:
        return success_response.jsonify({'status': "OK", 'code': 200, 'data': setup}), 200
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return error_response.jsonify({'status': "INTERNAL SERVER ERROR", 'code': 500, 'data': str(e)}), 500
