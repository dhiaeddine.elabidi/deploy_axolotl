"""
File: rsva.py
RSVA billing api routes definitions
"""
from app.utilities.logger import *
from app.models.rsva_invoice_model import RSVAInvoiceModel
from app.models.response_model import *
from flask import Blueprint, request
import json
import traceback


# create a new blueprint for the controller
api_billing_rsva = Blueprint('api_billing_rsva', __name__)


# define the routes
@api_billing_rsva.route('/numbers', methods=['POST'])
def numbers_invoice():
    try:
        data = json.loads(request.data)
        if type(data) is list:
            for d in data:
                if 'numbers' not in data:
                    raise TypeError("Missing 'numbers' parameter in request body.")
                if 'month' not in data:
                    raise TypeError("Missing 'month' parameter in request body.")
                if 'year' not in data:
                    raise TypeError("Missing 'year' parameter in request body.")
        else:
            raise TypeError("Bad request body data type.")
        billings = RSVABillingModel.numbers_invoice(numbers=data, month=data['month'], year=data['year'])
        billings = [
            {
                "number": "",
                "country_code": "",
                "nb_calls": 5,
            }
        ]
        return success_response.jsonify({'status': "OK", 'code': 200, 'data': billings}), 200
    except TypeError as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return error_response.jsonify({'status': "UNPROCESSABLE ENTITY", 'code': 422, 'data': str(e)}), 422
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return error_response.jsonify({'status': "INTERNAL SERVER ERROR", 'code': 500, 'data': str(e)}), 500


@api_billing_rsva.route('/user', methods=['POST'])
def user_invoice():
    try:
        data = json.loads(request.data)
        billing = [
            {
                "number": "",
                "country_code": "",
                "nb_calls": 5,
            }
        ]
        return success_response.jsonify({'status': "OK", 'code': 200, 'data': billing}), 200
    except Exception as e:
        log_error(
            message=str(e),
            description=traceback.format_exc(),
            ip_address=request.remote_addr
        )
        return error_response.jsonify({'status': "INTERNAL SERVER ERROR", 'code': 500, 'data': str(e)}), 500
