"""
File: rsva_billing.py
Synchronizing script
"""
import psycopg2
import traceback
import sys


rsva_db = {
    'user': "dynarsva",
    'password': "Zsa9c8r2019",
    'host': "rsva.yootel.io",
    'port': 5432,
    'database': "fusionpbx"
}

axolotl_db = {
    'user': "postgres",
    'password': "@Zsa9c8r2@!*$",
    'host': "185.222.62.217",
    'database': "axolotl_db"
}

# queries
ALL_NUMBERS = "SELECT number.country_code, number.number, rsva_tariff_client.call_rate, rsva_tariff_client.minute_rate, rsva_tariff_client.interval_1, rsva_tariff_client.interval_n, rsva_tariff_client.price_interval_1, rsva_tariff_client.price_interval_n, number.id FROM number INNER JOIN rsva_tariff ON number.rsva_tariff_id=rsva_tariff.id INNER JOIN rsva_tariff_client on rsva_tariff.rsva_tariff_client_id=rsva_tariff_client.id"
RSVA_CDR = "SELECT billsec FROM v_xml_cdr WHERE billsec>0 and destination_number='{0}' and EXTRACT(MONTH FROM end_stamp)={1} and EXTRACT(YEAR FROM end_stamp)={2}"
RSVA_NUMBER_INVOICE = "INSERT INTO rsva_number_invoice (num_calls, duration_calls, value, period_month, period_year, number_id) VALUES ({0}, {1}, {2}, {3}, {4},'{5}')"
CALCUL_USER_INVOICE = "SELECT number.user_id, sum(rsva_number_invoice.num_calls), sum(rsva_number_invoice.duration_calls), sum(rsva_number_invoice.value) FROM number INNER JOIN rsva_number_invoice ON rsva_number_invoice.number_id = number.id WHERE user_id IS NOT null AND rsva_number_invoice.period_month={0} AND rsva_number_invoice.period_year={1} GROUP BY number.user_id"
INSERT_USER_INVOICE = "INSERT INTO rsva_user_invoice (num_calls, duration_calls, pre_value, value, period_month, period_year, paid, user_id, created_at) VALUES ({0}, {1}, {2}, {2}, {3}, {4}, true, '{5}', now() at time zone 'utc')"
# create databases connection
try:
    rsva_connection = psycopg2.connect(user=rsva_db['user'],
                                       password=rsva_db['password'],
                                       host=rsva_db['host'],
                                       port=rsva_db['port'],
                                       database=rsva_db['database'])
    rsva_cursor = rsva_connection.cursor()
    axolotl_connection = psycopg2.connect(user=axolotl_db['user'],
                                          password=axolotl_db['password'],
                                          host=axolotl_db['host'],
                                          database=axolotl_db['database'])
    axolotl_cursor = axolotl_connection.cursor()
    # execution
    month = sys.argv[1]
    year = sys.argv[2]
    axolotl_cursor.execute(ALL_NUMBERS)
    numbers = axolotl_cursor.fetchall()
    for number in numbers:
        tariff = {
            'number': number[0]+number[1],
            'call_rate': number[2],
            'interval_1': number[4],
            'interval_n': number[5],
            'price_interval_1': number[6],
            'price_interval_n': number[7]
        }
        rsva_cursor.execute(RSVA_CDR.format(tariff['number'],  month, year))
        rows = rsva_cursor.fetchall()
        sum_values = 0
        sum_bill_sec = 0
        for row in rows:
            bill_sec = int(row[0])
            sum_bill_sec += bill_sec
            if bill_sec <= tariff['interval_1']:
                value = tariff['call_rate'] + \
                        (tariff['price_interval_1']/60)*bill_sec
            else:
                value = tariff['call_rate'] + \
                        (tariff['price_interval_1']/60)*tariff['interval_1'] + \
                        (tariff['price_interval_n']/60)*(bill_sec-tariff['interval_1'])
            sum_values += value
        # print(RSVA_NUMBER_INVOICE.format(len(rows), sum_bill_sec, sum_values, int(month), int(year), number[8]))
        axolotl_cursor.execute(RSVA_NUMBER_INVOICE.format(len(rows), sum_bill_sec, sum_values, int(month), int(year), number[8]))
        axolotl_connection.commit()
        print("number: ", tariff['number'], "||billsec:", sum_bill_sec, "||num calls:", len(rows), "||period: ", str(month)+"-"+str(year), "||total value:", round(sum_values, 2))
    axolotl_cursor.execute(CALCUL_USER_INVOICE.format(month, year))
    user_invoices = axolotl_cursor.fetchall()
    for user_invoice in user_invoices:
        axolotl_cursor.execute(INSERT_USER_INVOICE.format(user_invoice[1], user_invoice[2], user_invoice[3], month, year, user_invoice[0]))
        axolotl_connection.commit()
except psycopg2.Error as error:
    print("Error while fetching data from PostgreSQL:", error)
except Exception as e:
    print(traceback.format_exc())
finally:
    # closing database connection
    if rsva_connection:
        rsva_cursor.close()
        rsva_connection.close()
    if axolotl_connection:
        axolotl_cursor.close()
        axolotl_connection.close()
    print("Billing updated and closed.")
