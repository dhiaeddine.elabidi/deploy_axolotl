"""
File: db_migrate.py
migrate database
"""
from app.models.database.tables.user import User
from app.models.database.tables.category import Category
from app.models.database.tables.user_detail import UserDetail
from app.models.database.tables.log import Log
from app.models.database.tables.currency import Currency
from app.models.database.tables.service import Service
from app.models.database.tables.user_service import UserService
from app.models.database.tables.number import Number
from app.models.database.tables.rsva_tariff_end_user import RsvaTariffEndUser
from app.models.database.tables.rsva_tariff_client import RsvaTariffClient
from app.models.database.tables.rsva_tariff import RsvaTariff
from app.models.database.tables.rsva_user_invoice import RsvaUserInvoice
from app.models.database.tables.rsva_number_invoice import RsvaNumberInvoice
from app.models.database.tables.rsva_invoice import RsvaInvoice
from app.models.database.tables.ticket import Ticket
from app import db

# drop all previous data definition
db.drop_all()
# create all database tables
db.create_all()
