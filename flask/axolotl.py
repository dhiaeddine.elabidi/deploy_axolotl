"""
File axolotl.py
Main python entry point
Application serve web application and Rest API
To customize change the configuration files
"""
from app.utilities.configuration import configuration
from app.utilities.logger import *
from app import app
import traceback

# define configuration
config = configuration()

# main program definition
if __name__ == '__main__':
    try:
        HOST = config['connection']['host']
        PORT = config['connection']['port']
        DEBUG = config['development']['debug']
        log_info(
            message="Application running",
            description="options HOST="+HOST+" PORT="+str(PORT)+" DEBUG="+str(DEBUG))
        app.run(host=HOST, port=PORT, debug=DEBUG)
    except Exception as e:
        log_fatal(
            message=str(e),
            description=traceback.format_exc()
        )
